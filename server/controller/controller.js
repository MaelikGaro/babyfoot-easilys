const  send  = require('express/lib/response');

const pg = require('pg').Pool;

const pool = new pg({
    user: 'testEasilys',
    host: 'localhost',
    database: 'testEasilys',
    password: 'easilys',
    port: 5432,
  });

exports.getAllGames = function(req, res){
       
    pool.query('SELECT * FROM games')
        .then(pool => {
            res.send(pool.rows);
        })
        .catch()
  }


exports.createGame = function(req,res){


    const player1 = req.player1;
    const player2 = req.player2;
    const winner = null;
    const status = 0;

    const queryText = 'INSERT INTO games (player1, player2,winner,status) VALUES ($1, $2, $3, $4) RETURNING id_game';

    pool.query(queryText, [player1, player2,winner,status], (err,result) =>{
        if(err) throw err
        var lastId = result.rows[0].id_game;
        return res(lastId);
    })
}

exports.deleteGame = function(req, res){
    const id = req.id;

    pool.query('DELETE FROM games WHERE id_game = $1', [id],(err,result) =>{
        if(err) throw err
    })
}

exports.updateGame = function(req, res){


    const id = req.id_game;
    const winner = req.winner;
    const status = true;

    pool.query('UPDATE games SET winner = $1, status = $2 WHERE id_game = $3', [winner,status,id],(err,result) =>{
        if(err) throw err
        const data = {id : id, winner : winner, status : status};
    })
}

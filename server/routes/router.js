const express = require('express');
const route = express.Router();

const services = require('../services/render');
const controller = require('../controller/controller');


route.get('/',services.read);
route.post('/',services.create);


//API

route.get('/games',controller.getAllGames);
route.post('/games',controller.createGame);
route.delete('/games/:id',controller.deleteGame);
route.put('/games/:id',controller.updateGame);

module.exports = route

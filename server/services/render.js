const axios = require('axios');

exports.read = (req, res) => {
    axios.get('http://localhost:3000/games')
        .then(function(response){

            gamesNotFinished = new Array();
            gamesFinished = new Array();

            for(let i = 0 ; i < response.data.length; i++)
            {
                if(response.data[i].status === false){
                    gamesNotFinished.push(response.data[i]);
                }else{
                    gamesFinished.push(response.data[i]);
                }                
            }
            console.log(gamesFinished)
            res.render('index', { gamesNotFinished : gamesNotFinished, gamesFinished : gamesFinished, counter : gamesNotFinished.length });
        })
        .catch(err =>{
            res.send(err);
        })    
}

exports.create = (req, res) =>{
    if(Object.keys(req.body).length === 0 && req.body.constructor === Object){
        res.redirect('/');
        return;
    }
    axios.post('http://localhost:3000/games', {
            player1: req.body.player1,
            player2: req.body.player2,
            status:0
        })
        .then(function(){
            res.redirect('/');
        })
        .catch(err =>{
            res.send(err);
        })
}



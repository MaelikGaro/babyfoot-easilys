--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2
-- Dumped by pg_dump version 14.2

-- Started on 2022-05-30 13:00:54

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 209 (class 1259 OID 16455)
-- Name: games; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.games (
    id_game integer NOT NULL,
    player1 character(100) NOT NULL,
    player2 character(100) NOT NULL,
    winner character(100),
    status boolean NOT NULL
);


ALTER TABLE public.games OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 16488)
-- Name: games_id_game_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.games ALTER COLUMN id_game ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.games_id_game_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 3305 (class 0 OID 16455)
-- Dependencies: 209
-- Data for Name: games; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.games (id_game, player1, player2, winner, status) FROM stdin;
\.


--
-- TOC entry 3312 (class 0 OID 0)
-- Dependencies: 210
-- Name: games_id_game_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.games_id_game_seq', 1, false);


--
-- TOC entry 3165 (class 2606 OID 16459)
-- Name: games game_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games
    ADD CONSTRAINT game_pkey PRIMARY KEY (id_game);


-- Completed on 2022-05-30 13:00:54

--
-- PostgreSQL database dump complete
--


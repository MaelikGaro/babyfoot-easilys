# babyfoot-easilys

L'application permet de gérer les parties de babyfoot. Elle permet d'en créer, d'en supprimer et d'indiquer le nom du gagnant. De plus, l'utilisateur a accès à un chat pour discuter avec les autres utilisateurs connectés
## Installation

Pour installer le projet, il suffit de le cloner et ensuite d'installer les dépendances.

```bash
git clone https://gitlab.com/MaelikGaro/babyfoot-easilys.git
cd babyfoot-manager-easilys
npm install
```
### Liste des dépendances

Voici la liste des dépendances :
*express : Express.js est un framework pour construire des applications web basées sur Node.js.
*pg : Il permet de se connecter à une base de données PostgreSQL et ainsi pouvoir communiquer avec elle.
*socket.io : Il permet une communication bidirectionnelle, entre serveur et client,  en temps réel basées sur les évènements.
*axios : Axios est un clietn HTTP et il utilise XMLHttpRequests, ce qui signifie qu'il permet de faire des requêtes http.
*body-parser : Il permet de traiter les données des requêtes HTTP Post.
*ejs : C'est un moteur de templates qui permet de générer qui permet de modifier les vues avec les paramètres fournis par le serveur node.


## Base de données

Le paramétrage  de la connection au serveur de base de données s'effectue dans le server/controller/controller.js dans la variable pool.

```bash
const pool = new pg({
    user: 'testEasilys',
    host: 'localhost',
    database: 'testEasilys',
    password: 'easilys',
    port: 5432,
  });
```

- user : Le nom d'utilisateur pour se connecter à la base de données.
- host : L'adresse IP du serveur de base de données
- database : Le nom de base de données.
- password : Le mot de passe de la base de données.
- port : Le port sur lequel la base de données écoute.

### Structures des données

La base de données comporte une seule table `Games` :

| Attribut                        | Type      |
| :------------------------------ | :-------  |
| `id_game` (PK)                  | `INTEGER`     |
| `player1` NOT NULL              | `CHARACTER (100)`     |
| `player2` NOT NULL              | `CHARACTER (100)`     | 
| `id_winner`                     | `CHARACTER (100)`     | 
| `status` NOT NULL               | `BOOLEAN`     | 

## Utilisation

### Gestion des parties

- Création d'une partie : L'utilisateur doit rentrer le nom des joueurs puis appuyer sur le bouton "LANCER". La partie sera enregistrée dans la section des "parties en cours".
![Ajouter une partie : partie 1](documentation/creation.png)
![Ajouter une partie : partie 2](documentation/creation2.png)

- Suppression d'une partie : L'utilisateur peut supprimer une partie en cliquant sur le bouton rouge correspondant à la partie.
![Supprimer une partie](documentation/suppression.png)

- Modification d'une partie :  L'utilisateur peut sélectionner le gagnant de la partie et cliquant sur celui-ci. La partie va donc s'enregistrer dans la section "parties terminées".
![Modifier une partie : partie 1](documentation/modification.png)
![Modifier une partie : partie 2](documentation/modification2.png)

- utilisation du chat : L'utilisateur appuie sur le bouton chat pour le faire apparaitre.
![utilisation chat](documentation/chat.png)


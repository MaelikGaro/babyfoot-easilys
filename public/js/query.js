if(window.location.pathname == "/"){
    function delete_game(id)
    {
        var id = parseInt(id);

        if(confirm("Voulez-vous supprimer cette partie ?")){
            var url = "http://localhost:3000/games/"+id;

            var xhr = new XMLHttpRequest();
            xhr.open("DELETE", url);

            xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status == "200") {
                console.log(xhr.responseText);
            }};

            xhr.send();
            deleteG(id);
        }
        
    }

    function update_winner(value)
    {
        var id = value.substr(8);
        var player = value.substr(0,7);
        var idDiv = player+'Game'+id;
        var winner = document.getElementById(idDiv).textContent;

        if(confirm("Est-ce le bon gagnant ?")){
            var data = {};
            data.id_game = id;
            data.winner = winner;
            var json = JSON.stringify(data);
            var url = "http://localhost:3000/games/"+id;

            var xhr = new XMLHttpRequest();
            xhr.open("PUT", url);
            xhr.setRequestHeader('Content-type',"application/json;charset=UTF-8");

            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && xhr.status === "200") {
                    console.log(xhr.responseText);
                }
            }

            xhr.send();
            updateG(data);
        }

    }
}


var socket = io();


        socket.on("gameCreated", function(data) {            
            
            var id = data.id;
            var idGame = "game"+id;


            var newDiv = document.createElement("div");
            newDiv.setAttribute("id",idGame);
            newDiv.setAttribute("class","elementCard");

            var btnPLayer1 = document.createElement('button');
            btnPLayer1.setAttribute("id","player1Game"+id);
            btnPLayer1.setAttribute("value","player1-"+id);
            btnPLayer1.setAttribute("class","player1btn p1Selected");
            btnPLayer1.setAttribute("onClick","update_winner(this.value)");
            btnPLayer1.innerHTML = data.player1;
            newDiv.appendChild(btnPLayer1);

            var versus = document.createElement('p');
            versus.setAttribute("class","versus");
            versus.innerHTML = "VS";
            newDiv.appendChild(versus);

            var btnPLayer2 = document.createElement('button');
            btnPLayer2.setAttribute("id","player2Game"+id);
            btnPLayer2.setAttribute("value","player2-"+id);
            btnPLayer2.setAttribute("class","player2btn p2Selected");
            btnPLayer2.setAttribute("onClick","update_winner(this.value)");
            btnPLayer2.innerHTML = data.player2;
            newDiv.appendChild(btnPLayer2);

            var btnSupp = document.createElement('button');
            btnSupp.setAttribute("id",id);
            btnSupp.setAttribute("class","btnSupp");
            btnSupp.setAttribute("onClick","delete_game(this.id)");
            btnSupp.innerHTML = "X";
            newDiv.appendChild(btnSupp);

            
            var currentDiv = document.getElementById('gamesNotFinished');
            currentDiv.appendChild(newDiv);

            document.getElementById("playerOne").value = "";
            document.getElementById("playerTwo").value = "";

            counter = document.getElementById('gamesNotFinished').childElementCount;
            
            document.getElementById("counter").innerHTML = counter; 


        });



        function createG(){

            var player1 = document.getElementById("playerOne").value;
            var player2 = document.getElementById("playerTwo").value;
            data = {player1 : player1, player2: player2};

            socket.emit('createGame', data);
        }



        socket.on("gameDeleted", function(data) {

            const game = document.getElementById("game" + data.id);

            game.parentNode.removeChild(game);


            counter = document.getElementById('gamesNotFinished').childElementCount;
            
            document.getElementById("counter").innerHTML = counter;       

        });
        
        function deleteG(id){

            var idGame = id;
            data = {id : idGame};

            socket.emit('deleteGame', data);
        }

        socket.on("gameUpdated", function(data) {

            const game = document.getElementById("game" + data.id_game);

            var currentDiv = document.getElementById('gamesFinished');

            currentDiv.appendChild(game);

            player1 = document.getElementById("player1Game" + data.id_game);
            player2 = document.getElementById("player2Game" + data.id_game);

            if(player1.textContent === data.winner){
                player1.setAttribute("class", "player1btn winner1");
            }else if(player2.textContent === data.winner){
                player2.setAttribute("class", "player2btn winner2");
            }


            counter = document.getElementById('gamesNotFinished').childElementCount;
            
            document.getElementById("counter").innerHTML = counter;      

        });

        function updateG(data){
            socket.emit('updateGame',data);
        }

            var sendMsg = document.getElementById("send-msg");
            sendMsg.addEventListener("click", () => {
                var msg = document.getElementById("msg-input").value;
                socket.emit('updateChat',msg)

                var divMsg = document.createElement('div');
                divMsg.setAttribute("class","my-msg msg");

                var messages = document.getElementById("messages");
                messages.appendChild(divMsg)

                var content = document.createElement('div');
                content.setAttribute("class","content");
                divMsg.appendChild(content);

                var text = document.createElement('div');
                text.setAttribute("class","my-text");
                text.innerHTML = msg;                
                content.appendChild(text);

            })

        socket.on("chatUpdated", function(msg) {

            var divMsg = document.createElement('div');
            divMsg.setAttribute("class","other-msg msg");

            var messages = document.getElementById("messages");
            messages.appendChild(divMsg)

            var content = document.createElement('div');
            content.setAttribute("class","content");
            divMsg.appendChild(content);

            var text = document.createElement('div');
            text.setAttribute("class","other-text");
            text.innerHTML = msg;

            
            content.appendChild(text);    

        });
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');

const app = express();


const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);

const port = 3000;

app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/public',express.static(path.join(__dirname, '/public/')));



app.use('/', require('./server/routes/router'));

const controller = require("./server/controller/controller");



io.on('connection',(socket) => {
  console.log('a user connected');

  socket.on("createGame", (data) => {
    controller.createGame(data, function(id){
    const newGame = {player1 : data.player1, player2 : data.player2, id : id}
      io.emit("gameCreated", newGame);
    })
    
  });

  socket.on("deleteGame", (data) => {
    controller.deleteGame(data);
      io.emit("gameDeleted", data);
  });

  socket.on("updateGame", (data) => {
    controller.updateGame(data);
      io.emit("gameUpdated", data);
    
  });

  socket.on("updateChat",(msg)=>{
    io.emit("chatUpdated",msg)
  })

});



server.listen(port, () => {
  console.log(`App running on port ${port}.`)
})

